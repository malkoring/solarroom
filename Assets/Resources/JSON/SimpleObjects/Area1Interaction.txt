{
    "door001_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["양철 지붕에 회색 벽의 집이다."]
                    },
                    {
                        "type": "reset"
                    }
                ]
            }
        ]
    },
    "door002_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["<b><color=red>갈색</color></b> 지붕의 벽돌 집이다."]
                    },
                    {
                        "type": "soundeffect",
                        "name": "bark"
                    }
                ]
            }
        ]
    },
    "door003_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["회색 벽돌 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-29 라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "door004_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["갈색 벽돌에 큰 창문이 달린 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-30 이라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "door005_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["흰색 문에 회색 벽의 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-31 이라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "door006_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["나무 문의 벽돌 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-32 라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "door007_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["회색 벽에 물탱크가 있는 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-33 이라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "door008_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["양철 지붕에 철문이 달린 집이다."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["12-34 이라고 적혀있다."]
                    }
                ]
            }
        ]
    },
    "watertank_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["물탱크야. "]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["내가 목마르다고 해도 저긴 닿을 수 없어."]
                    }
                ]
            }
        ]
    },
    "trash001_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["아무 도움이 되지 않아."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["이건 쓸모없어."]
                    }
                ]
            }
        ]
    },
    "bicycle_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["삼만리를 갈 수 있다는 자전거야."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["내 것이 아닌걸."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["자전거는 탈 줄 몰라."]
                    }
                ]
            }
        ]
    },
    "obstruction_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["여긴 자동차도 없는데..."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["주차금지."]
                    }
                ]
            }
        ]
    },
    "trash002_area1": {
        "events": [
            {
                "order": "random",
                "events":
                [
                    {
                        "type": "dialogue",
                        "dialogue": ["뭔지 모르겠어. 그런데 만지고 싶지 않아."]
                    },
                    {
                        "type": "dialogue",
                        "dialogue": ["도움이 될 것 같지 않아."]
                    }
                ]
            }
        ]
    }
    
}