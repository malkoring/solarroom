﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundEffectsManager : MonoBehaviour {

	public List<AudioClip>	soundEffects;
	private static bool sExists;
	private static bool activated;
	private static int	indexOfSoundEffect;

	// Use this for initialization
	void Start () 
	{
		if(!sExists) {
			sExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy(gameObject);
		}
	}
	
	void Update()
	{
		if(activated)
		{
			AudioSource audiosource = GetComponent<AudioSource>();
			AudioClip audioclip = soundEffects[indexOfSoundEffect];
			StartCoroutine(Global.wait(audioclip.length/2));
			audiosource.PlayOneShot(audioclip);

			activated = false;
		}
	}

	public static void playSound(string objectname)
	{
		indexOfSoundEffect = Global.GetSoundEffect(objectname);
		activated = true;
	}
	
}
