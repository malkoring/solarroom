﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour {
	
	public static GameObject target;
	public static Animator anim;
	public static Vector3 after;
	public static float speed;
	private static bool activated;

	public static Vector3 faceTo;
	public static Vector3 normalVector;

	void Update()
	{
		if(activated)
		{
			if (
					Mathf.Abs(target.transform.position.x - after.x) < 0.3f
				&&	Mathf.Abs(target.transform.position.y - after.y) < 0.3f

				)  {
				anim.SetBool("PlayerMoving", false);
				PlayerController.autoMove = false;
				ScenarioQueue.isPlaying = true;
				activated = false;
				return;
			}
			
			Debug.Log(anim.GetFloat("LastMoveY"));

			anim.SetFloat("MoveX", faceTo.x);
			anim.SetFloat("MoveY", faceTo.y);
			anim.SetBool("PlayerMoving", true);
			anim.SetFloat("LastMoveX", faceTo.x);
			anim.SetFloat("LastMoveY", faceTo.y);

			target.transform.Translate(normalVector * Time.deltaTime * speed / 3);
		}
	}

	public static void MakeMove(string name, Vector2 direction, Vector2 movement, float _speed) {
		FindAnimator(name);

		faceTo = new Vector3(direction.x, direction.y, 0);

		float x = target.transform.position.x + movement.x;
		float y = target.transform.position.y + movement.y;
		after = new Vector3(x, y, target.transform.position.z);

		normalVector = movement.normalized;

		activated = true;
		PlayerController.autoMove = true;
		speed = _speed;
	}

	public static void CharacterSitDown(string name)
	{
		FindAnimator(name);

		bool isSittingDown = anim.GetBool("PlayerSitting");
		anim.SetBool("PlayerSitting", !isSittingDown);

		ScenarioQueue.isPlaying = true;
	}

	public static void CharacterHandsIn(string name)
	{
		FindAnimator(name);

		bool isHandingIn = anim.GetBool("PlayerHanding");
		anim.SetBool("PlayerHanding", !isHandingIn);

		ScenarioQueue.isPlaying = true;
	}

	private static void FindAnimator(string objectName)
	{
		target = GameObject.Find(objectName);
		anim = target.GetComponent<Animator>();
	}
}
