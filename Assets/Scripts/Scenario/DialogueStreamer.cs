﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueStreamer : MonoBehaviour {

	public static string originalText;
	public static string printText;
	public static int printIndex;
	public static List<string> markupText;
	public static int m;

	public static void AssignText(string text) {
		printIndex = 0;
		m = 0;
		printText = "";
		markupText = new List<string>();
		originalText = text;
		string[] s = text.Split(' ');
		for(int i=0; i<s.Length; ++i)
		{
			if(s[i][0] == '<' && s[i][s[i].Length-1] == '>')
				markupText.Add(s[i]);
		}
	}

	public static string ShowNextCharacter() {
		if(originalText[printIndex] == '<' && originalText[printIndex + markupText[m].Length-1] == '>')
		{
			printText += markupText[m];
			printIndex += markupText[m++].Length;
		}
		else 
		{
			printText += originalText[printIndex++];
		}

		return printText; 
	}
}
