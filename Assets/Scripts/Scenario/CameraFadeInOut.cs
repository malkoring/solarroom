﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFadeInOut : MonoBehaviour {

	private static bool fadein;
	private static bool fadeout;
	private static float alpha;
	private static float speed;
	public GameObject mask;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (fadein)
		{
			mask.SetActive(true);
			if(alpha <= 0)
			{
				fadein = false;
				ScenarioQueue.isPlaying = true;
				mask.SetActive(false);
			}

			alpha = Mathf.Clamp(alpha-Time.deltaTime/speed, 0, 1);
			mask.GetComponent<Image>().color = new Color(0, 0, 0, alpha);
		}

		if (fadeout)
		{
			mask.SetActive(true);
			if(alpha >= 1)
			{
				fadeout = false;
				ScenarioQueue.isPlaying = true;
				Global.itsTimeToMove = true;
			}

			alpha = Mathf.Clamp(alpha+Time.deltaTime/speed, 0, 1);
			mask.GetComponent<Image>().color = new Color(0, 0, 0, alpha);
		}
	}

	public static void FadeIn()
	{
		fadein = true;
		alpha = 1;
		speed = 1;
	}

	public static void FadeOut()
	{
		fadeout = true;
		alpha = 0;
		speed = 1;
	}

	public static void SlowFadeIn()
	{
		fadein = true;
		alpha = 1;
		speed = 3;
	}

	public static void SlowFadeOut()
	{
		fadeout = true;
		alpha = 0;
		speed = 3;
	}
}
