﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAutoMove : MonoBehaviour {

	private static Vector3 destination;
	private static Vector3 faceTo;
	private static bool activation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Global.cameraAutoMove) {

			if(	   Mathf.Abs(destination.x - gameObject.transform.position.x) < 0.1f
				&& Mathf.Abs(destination.y - gameObject.transform.position.y) < 0.1f) 
			{
				Global.cameraAutoMove = activation;
				ScenarioQueue.isPlaying = true;
			}

			transform.Translate(faceTo * Time.deltaTime);

		}
	}

	public static void Init(float _x, float _y, float _amount, bool _activation)
	{
		GameObject camera = GameObject.Find("Main Camera");
		float x = camera.transform.position.x + _x * _amount;
		float y = camera.transform.position.y + _y * _amount;

		activation = _activation;

		faceTo = new Vector3(_x, _y, 0);
		destination = new Vector3(x, y, camera.transform.position.z);
		Global.cameraAutoMove = true;
	}
}
