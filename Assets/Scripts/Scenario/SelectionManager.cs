﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviour {

	public GameObject selectionBox;
	public Text[]	selectionTexts;
	
	private static bool sExists;
	public static bool selectionActive;
	private static Queue<JSONObject> selectedScenario;
	private static List<JSONObject>	 listOfScenario;
	private static List<JSONObject>	 listOfDescriptions;

	// Use this for initialization
	void Start () 
	{
		if(!sExists) {
			sExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(!selectionActive)
			selectionBox.SetActive(false);
		else {
			selectionBox.SetActive(true);
			for(int i=0; i<3; ++i)
			{
				selectionTexts[i].text = listOfDescriptions[i].str;
			}
		}
	}
	public void Selection(int selection) {
		selectedScenario = new Queue<JSONObject>(listOfScenario[selection].list);
		while(ScenarioQueue.queue.Count > 0)
		{
			selectedScenario.Enqueue(ScenarioQueue.queue.Dequeue());
		}

		while(selectedScenario.Count > 0)
		{
			ScenarioQueue.queue.Enqueue(selectedScenario.Dequeue());
		}

		selectionActive = false;
		ScenarioQueue.isPlaying = true;
	}
	
	public static void ShowSelection(List<JSONObject> descriptions, List<JSONObject> selects) 
	{
		selectionActive = true;

		listOfDescriptions = descriptions;
		listOfScenario = selects;
		
	}
}
