﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioQueue : MonoBehaviour {

	public static Queue<JSONObject> queue;
	public static bool isPlaying;
	public static int evidence;
	private static bool qExists;

	// Use this for initialization
	void Start () 
	{
		queue = new Queue<JSONObject>();
		isPlaying = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(isPlaying) {
			isPlaying = false;

			if( queue.Count == 0 ) {
				VirtualJoystick.MakeOpaque();
				PlayerPrefs.SetInt("DontTouch", 0);
			}

			if( queue.Count > 0) 
			{
				JSONObject e = queue.Dequeue();
				PlayerPrefs.SetInt("DontTouch", 1);
				Eval(e);
			}

		}
	}

	void Eval(JSONObject e)
	{
		switch (e["type"].str) {
		case "skip":
			ScenarioQueue.isPlaying = true;
			break;
		case "clear": 
			ScenarioQueue.queue.Clear();
			ScenarioQueue.isPlaying = true;
			break;
		case "dialogue":
			List<JSONObject> s = e["dialogue"].list;
			DialogueManager.showDialogue(s, s.Count);
			break;
		case "character_move":
			ImplementCharacterMove(e["character_move"]);
			break;
		case "progress":
			++PlayData.progress[(int)e["quest"].n];
			ScenarioQueue.isPlaying = true;
			
			break;
		case "hands_in":
			CharacterMove.CharacterHandsIn("Player");
			break;
		case "get_inventory_1":
			PlayData.progress[evidence] += 1;
			PlayData.inventories[PlayData.numOfInventory++] = (int)e["inventory_id"].i;
			ScenarioQueue.isPlaying = true;
			break;
		case "get_inventory_2":
			PlayData.progress[evidence] += 2;
			PlayData.inventories[PlayData.numOfInventory++] = (int)e["inventory_id"].i;
			ScenarioQueue.isPlaying = true;
			break;
		case "soundeffect":
			SoundEffectsManager.playSound(e["name"].str);
			break;
		case "sit":
			CharacterMove.CharacterSitDown("Player");
			break;
		case "reset_inventory":
			PlayData.inventories[0] = 0;
			PlayData.inventories[1] = 0;
			PlayData.numOfInventory = 0;
			ScenarioQueue.isPlaying = true;
			break;
		case "get_lightbulb":
			PlayData.lightbulbs[(int)e["number"].n-1] = true;
			ScenarioQueue.isPlaying = true;
			break; 
		case "camera":
			ImplementCameraMove(e);
			break;
		case "select":
			SelectionManager.ShowSelection(e["descriptions"].list, e["results"].list);
			break;
		case "save":
			PlayData.Save();
			break;
		case "reset":
			PlayData.Reset();
			break;
		case "wait":
			StartCoroutine(Global.wait(e["duration"].f));
			break;
		case "switch":
			PlayData.conditions[(int)e["number"].i] ^= true;
			ScenarioQueue.isPlaying = true;
			break;
		}
			
	}

	void ImplementCameraMove(JSONObject e)
	{
		float x, y, amount;
		switch(e["action"].str)
		{
			case "fadein":	CameraFadeInOut.FadeIn();	break;
			case "fadeout":	CameraFadeInOut.FadeOut();	break;
//			case "shake": CameraShake.Init(e["duration"].n); break;
			case "move": 
				JSONObject j = e["move"];
				amount = j["amount"].n;
				switch(j["vertical"].str) 
				{
					case "up":		y= 1; break;
					case "down":	y=-1; break;
					default:		y= 0; break;
				}
								
				switch(j["horizontal"].str) 
				{
					case "right":	x= 1; break;
					case "left":	x=-1; break;
					default:		x= 0; break;
				}
							
				CameraAutoMove.Init(x, y, amount, j["activation"].b);
				break;
		}

	}
	void ImplementCharacterMove(JSONObject j)
	{
		float x, y, faceX, faceY;
		x = j["x"].f;
		y = j["y"].f;
		faceX = Mathf.Clamp(x, -1, 1);
		faceY = Mathf.Clamp(y, -1, 1);

		CharacterMove.MakeMove(j["actor"].str ,new Vector2(faceX, faceY), new Vector2(x, y), j["speed"].f);
	}
}
