using System.Collections.Generic;
using UnityEngine;

public class ObjectInteraction : MonoBehaviour {

    public int evidenceID;
    protected string loadfile;

    [HideInInspector]
    public int interactionType;

    protected bool collisionStarted;

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.name == "Player") {
            collisionStarted = true;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if(other.gameObject.name == "Player") {
            collisionStarted = false;
        }
    }

	// Use this for initialization
	void Start () {

	}

    virtual public void SetEvidenceID() { }

	// Update is called once per frame
	void Update () {
		if( !DialogueManager.dialogueActive 
            && collisionStarted 
            && PlayerPrefs.GetInt("Interaction") == 1 )
        {
            TextAsset t = Resources.Load(loadfile) as TextAsset;
            JSONObject j = new JSONObject(t.text);
            List<JSONObject> l = j[gameObject.name]["events"].list;

            SetEvidenceID();

            JSONObject k = l[PlayData.progress[ScenarioQueue.evidence]];

            

            List<JSONObject> w = k["events"].list;
            if (k["order"].str == "sequential")
            {
                ScenarioQueue.queue = new Queue<JSONObject>(w);
            }
            else
            {
                ScenarioQueue.queue = new Queue<JSONObject>();
                ScenarioQueue.queue.Enqueue(w[Random.Range(0, w.Count)]);
            }

            ScenarioQueue.isPlaying = true;
            VirtualJoystick.MakeTransparent();
        } 
	}

}
