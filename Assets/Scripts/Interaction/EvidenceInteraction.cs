﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceInteraction : ObjectInteraction {

	// Use this for initialization
	void Start () {
		loadfile = "JSON/lightbulb" + evidenceID;
	}
	
	override public void SetEvidenceID() {
		ScenarioQueue.evidence = evidenceID;
	}
}
