﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

	public GameObject dialogueBox;
	public Text dialogueText;
	private static int dialogueIndex;
	private static List<JSONObject> dialoguesToShow;

	private static bool dExists;
	private static int dialogueLength;
	private static int dialogueTextLength;

	// Use this for initialization
	void Start () 
	{
		if(!dExists) {
			dExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(!dialogueActive)
			dialogueBox.SetActive(false);
		else {
			dialogueBox.SetActive(true);
			if(DialogueStreamer.printIndex < dialogueTextLength )
				dialogueText.text = DialogueStreamer.ShowNextCharacter();
		}
	}

	public void showNextDialogue() 
	{
		if(dialogueIndex >= dialogueLength) {
			dialogueActive = false;
			ScenarioQueue.isPlaying = true;
		} else {
			DialogueStreamer.AssignText(dialoguesToShow[++dialogueIndex].str);
			dialogueTextLength = dialoguesToShow[dialogueIndex].str.Length;
		}
	}

	public static bool dialogueActive;
	public static void showDialogue(List<JSONObject> dialogue, int length) 
	{
		dialogueActive = true;
		dialoguesToShow = dialogue;
		dialogueLength = length-1;
		dialogueIndex = 0;

		DialogueStreamer.AssignText(dialoguesToShow[0].str);
		dialogueTextLength = DialogueStreamer.originalText.Length;
	}
}
