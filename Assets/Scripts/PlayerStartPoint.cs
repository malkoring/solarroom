﻿using UnityEngine;

public class PlayerStartPoint : MonoBehaviour {

	private GameObject player;
	private CameraController cameraControl;
	private PlayerController playerControl;

	

	// Use this for initialization
	void Start () {
		Vector2 coordinate = (PlayData.onLoad) ? PlayData.position : Global.GetStartPosition();
		Vector2 direction = (PlayData.onLoad) ? PlayData.faceTo : Global.GetStartDirection();
		PlayData.onLoad = false;
		CameraController.cameraFix = Global.FixCamera();
		CameraFadeInOut.FadeIn();

		player = GameObject.Find("Player");
		player.transform.position = coordinate;
		
		PlayerController.lastMove = direction;

		cameraControl = FindObjectOfType<CameraController>();
		cameraControl.transform.position = new Vector3(
			coordinate.x, coordinate.y, cameraControl.transform.position.z);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

}

