﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundMovie : MonoBehaviour {

	MovieTexture mv;
	// Use this for initialization
	void Start () {
		mv = (MovieTexture)GetComponent<RawImage>().mainTexture;
		mv.Play();
		mv.loop = true;
//		Handheld.PlayFullScreenMovie("");
	}
	
} 
