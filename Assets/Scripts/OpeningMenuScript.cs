﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class OpeningMenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			gameObject.GetComponent<Image>().color = new Color(1,1,1,0.5f);
		} else {
			gameObject.GetComponent<Image>().color = new Color(1,1,1,1);
		}

	}

	public void StartGame() {

		PlayData.Load();
		Global.portalInfo = PlayData.mapID;
		SceneManager.LoadScene(Global.LoadMap());
	}

	public void ContinueGame() {
		
	}
}
