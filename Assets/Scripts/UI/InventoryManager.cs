﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

	public GameObject[] inventories;
	public Texture[]		images;

	// Update is called once per frame
	void Update () {
		inventories[0].GetComponent<RawImage>().texture = images[PlayData.inventories[0]];
		inventories[1].GetComponent<RawImage>().texture = images[PlayData.inventories[1]];
	}
}
