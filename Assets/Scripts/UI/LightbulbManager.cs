﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightbulbManager : MonoBehaviour {

	public GameObject[] lightbulbs;
	
	// Update is called once per frame
	void Update () {
		for(int i=0; i<9; ++i)
		{
			lightbulbs[i].GetComponent<RawImage>().color 
				= PlayData.lightbulbs[i] ? 
					new Color(1, 1, 1, 1) : 
					new Color(1, 1, 1, 0);
		}
	} 
}
