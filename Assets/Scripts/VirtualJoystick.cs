﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

	private static Image bgImg;
	private static Image joystickImg;
	private static Image directionImg;
	private static Image interactionButton;
	private static Vector3 inputVector;
	private static bool joystickExists;


	void Start() {
		PlayerPrefs.SetInt("DontTouch", 0);
		bgImg = transform.GetChild(0).GetComponent<Image>();
		joystickImg = transform.GetChild(0).GetChild(0).GetComponent<Image>();
		interactionButton = transform.GetChild(1).GetComponent<Image>();
		directionImg = transform.GetChild(0).GetChild(1).GetComponent<Image>();

		inputVector = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
	
	}

	public virtual void OnDrag(PointerEventData ped) {
		Vector2 pos;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(
			bgImg.rectTransform, ped.position,	ped.pressEventCamera, out pos
		)) {
			pos.x = pos.x / bgImg.rectTransform.sizeDelta.x;
			pos.y = pos.y / bgImg.rectTransform.sizeDelta.y;

			inputVector = new Vector3(pos.x*2, pos.y*2, -10);
			inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

			joystickImg.rectTransform.anchoredPosition =
				new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 0.7f),
							inputVector.y * (bgImg.rectTransform.sizeDelta.y / 0.7f));

			
		} 
	}

	public virtual void OnPointerDown(PointerEventData ped) {
		if(PlayerPrefs.GetInt("DontTouch") != 1)
			OnDrag(ped);
	}
	public virtual void OnPointerUp(PointerEventData ped) {
		inputVector = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public static float Horizontal() 
	{
		if(inputVector.x != 0)
			return inputVector.x;
		else
			return Input.GetAxis("Horizontal");
	}

	public static float Vertical()
	{
		if(inputVector.y != 0)
			return inputVector.y;
		else
			return Input.GetAxis("Vertical");
	}

	public static void MakeTransparent() {
		bgImg.color = new Color(bgImg.color.r, bgImg.color.g, bgImg.color.b, 0);
		joystickImg.color = new Color(joystickImg.color.r, joystickImg.color.g, 
								joystickImg.color.b, 0);
		directionImg.color = new Color(directionImg.color.r, directionImg.color.g, 
								directionImg.color.b, 0);
		interactionButton.color = new Color(interactionButton.color.r, 
											interactionButton.color.g, 
											interactionButton.color.b, 0);
	}

	public static void MakeOpaque() {
		bgImg.color = new Color(bgImg.color.r, bgImg.color.g, bgImg.color.b, 0);
		joystickImg.color = new Color(joystickImg.color.r, joystickImg.color.g, 
								joystickImg.color.b, 1);
		interactionButton.color = new Color(interactionButton.color.r, 
											interactionButton.color.g, 
											interactionButton.color.b, 1);
		directionImg.color = new Color(directionImg.color.r, directionImg.color.g, 
										directionImg.color.b, 1);
	}

	public static void init() {
		
	}
}
