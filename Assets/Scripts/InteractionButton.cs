﻿using UnityEngine;

public class InteractionButton : MonoBehaviour {

	public void StartInteraction() 
	{
		PlayerPrefs.SetInt("Interaction", 1);
	}

	void LateUpdate() 
	{
		PlayerPrefs.SetInt("Interaction", 0);
	}
}
