﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalController : MonoBehaviour {

	public	int 	levelToLoad;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Global.onMoveMap) {
			if(Global.itsTimeToMove) 
			{
				Global.itsTimeToMove = false;
				Global.onMoveMap = false;
				SceneManager.LoadScene(Global.LoadMap());
			} 
			
		}
	}

	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.name == "Player") {
			Global.portalInfo = levelToLoad;
			PlayData.mapID = levelToLoad;
			Global.onMoveMap = true;
			PlayerPrefs.SetInt("DontTouch", 1);
			CameraFadeInOut.FadeOut();
		}
	}
}
