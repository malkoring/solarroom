﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global {

	public static int portalInfo;
	public static bool itsTimeToMove;
	public static bool onMoveMap;
	public static bool cameraAutoMove;

	private static readonly int[] indexOfMap = {0, 1, 0, 2, 1, 3, 1, 4, 2, 5, 4, 2, 5, 6, 7, 6, 1, 7, 8, 5};
	public static readonly string[] MapToLoad = {"Home", "Area1", "Area2", "Area3", "Area4", "Area5", "Area6", "Area7", "Area8", "Area9"};
	private static readonly bool[] 	 whetherFixCamera = {true, false, false, false, false, false, false, false, false, false, false, false, false, false};
	private static readonly Dictionary<string, int> soundeffects = new Dictionary<string, int>
	{
		{"crow", 0}, {"crow1", 1}, {"crow2", 2}, {"crow3", 3}, {"crow4", 4}, {"crow5", 5}, {"crow6", 6}, {"crow7", 7}, {"crow8", 8}, {"crow9", 9}, 
/*		{"crow", 10}, {"crow", 11}, {"crow", 12}, {"crow", 13}, {"crow", 14}, {"crow", 15}, {"crow", 16}, {"crow", 17}, {"crow", 18}, {"crow", 19}, 
		{"crow", 20}, {"crow", 21}, {"crow", 22}, {"crow", 23}, {"crow", 24}, {"crow", 25}, {"crow", 26}, {"crow", 27}, {"crow", 28}, {"crow", 29},
		{"crow", 30}, {"crow", 31}, {"crow", 32}, {"crow", 33}, {"crow", 34}, {"crow", 35}, {"crow", 36}, {"crow", 37}, {"crow", 38}, {"crow", 39}, 
		{"crow", 40}, {"crow", 41}, {"crow", 42}, {"crow", 43}, {"crow", 44}, {"crow", 45}, {"crow", 46}, {"crow", 47}, {"crow", 48}, {"crow", 49} 
*/	};

	private static readonly Vector2[] StartingPosition = 
	{
		new Vector2(-4.1f, -1.0f),								// Initial
		new Vector2(-13.97f, -0.48f), 							// Home -> Area1
		new Vector2(-4.1f, -1.0f),								// Area1 -> Home
		new Vector2(0.4f, -4.43f),								// Area1 -> Area2
		new Vector2(5.4f, -0.75f),								// Area2 -> Area1
		new Vector2(-9.24f, -1.52f),							// Area1 -> Area3
		new Vector2(-16.21f, -2.35f),							// Area3 -> Area1
		new Vector2(-16.21f, -2.35f),							// Area2 -> Area4
		new Vector2(-16.21f, -2.35f),							// Area4 -> Area2
		new Vector2(-16.21f, -2.35f),							// Area4 -> Area5
		new Vector2(-16.21f, -2.35f),							// Area5 -> Area4
		new Vector2(-16.21f, -2.35f),							// Area5 -> Area2
		new Vector2(-16.21f, -2.35f),							// Area2 -> Area5
		new Vector2(-16.21f, -2.35f),							// Area1 -> Area6
		new Vector2(-16.21f, -2.35f),							// Area6 -> Area7
		new Vector2(-16.21f, -2.35f),							// Area7 -> Area6
		new Vector2(-16.21f, -2.35f),							// Area7 -> Area1
		new Vector2(-16.21f, -2.35f),							// Area1 -> Area7
		new Vector2(-16.21f, -2.35f),							// Area5 -> Area8
		new Vector2(-16.21f, -2.35f)							// Area8 -> Area5
	};

	private static readonly Vector2[] StartingDirection =  
	{
		new Vector2(0.0f, -1.0f), new Vector2(0.0f, -1.0f), new Vector2(0.0f, -1.0f),
		new Vector2(0.0f, -1.0f), new Vector2(0.0f, -1.0f), new Vector2(0.0f, -1.0f),
		new Vector2(1.0f, 0.0f)
	};

	public static string LoadMap()
	{
		return MapToLoad[indexOfMap[portalInfo]];
	}

	public static Vector2 GetStartPosition()
	{
		return StartingPosition[portalInfo];
	}

	public static Vector2 GetStartDirection()
	{
		return StartingDirection[portalInfo];
	}

	public static bool FixCamera() 
	{
		return whetherFixCamera[indexOfMap[portalInfo]];
	}

	public static int GetSoundEffect(string objectname) 
	{
		return soundeffects[objectname];
	}

	public static IEnumerator wait(float f)
	{
		yield return new WaitForSeconds(f);
		ScenarioQueue.isPlaying = true;
	}
}
