﻿using System.Collections;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayData {
	public static int[]  progress;
	// 9 Lightbulbs
	public static bool[] lightbulbs;
	public static int[]	 inventories;
	public static Vector2 position;
	public static Vector2 faceTo;
	public static int mapID;
	public static int numOfInventory;
	public static bool[] conditions;
	public static bool onLoad;

	public static void Save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		SaveData data = new SaveData();
		data.progress 	= progress;
		data.inventories= inventories;
		data.lightbulbs = lightbulbs;
		data.positionX	= position.x;
		data.positionY	= position.y;
		data.faceToX 	= faceTo.x;
		data.faceToY	= faceTo.y;
		data.mapID 		= mapID;
		data.numOfInventory = numOfInventory;
		data.conditions = conditions;
		data.onLoad 	= true;

		bf.Serialize(file, data);
		file.Close();

		Debug.Log("Saving Data...");
		Debug.Log(progress);
		Debug.Log(lightbulbs);

		ScenarioQueue.isPlaying = true;
	}

	public static void Load() {
		if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			SaveData data = (SaveData)bf.Deserialize(file);
			file.Close();

			inventories = data.inventories;
			progress	= data.progress;
			lightbulbs	= data.lightbulbs;
			position	= new Vector2(data.positionX, data.positionY);
			faceTo		= new Vector2(data.faceToX, data.faceToY);
			mapID		= data.mapID;
			numOfInventory = data.numOfInventory;
			conditions	= data.conditions;
			onLoad		= data.onLoad;
		} else {
			progress 	= new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			lightbulbs 	= new bool[] {false, false, false, false, false, false, false, false, false};
			position	= new Vector2(0, 0);
			faceTo		= new Vector2(0, -1);
			inventories = new int[] {0, 0};
			mapID		= 0;
			numOfInventory = 0;
			onLoad 		= false;
			conditions 	= new bool[100];
		}


		Debug.Log("Loading Data...");
		Debug.Log(progress);
		Debug.Log(lightbulbs);
	}

	public static void Reset() {
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
			File.Delete(Application.persistentDataPath + "/playerInfo.dat");

		ScenarioQueue.isPlaying = true;
	}
}

[Serializable]
public class SaveData {
	public int[]  progress;
	public bool[] lightbulbs;
	public int[]  inventories;
	public float positionX;
	public float positionY;
	public float faceToX;
	public float faceToY;
	public int mapID;
	public int numOfInventory;
	public bool[] conditions;
	public bool onLoad;
}