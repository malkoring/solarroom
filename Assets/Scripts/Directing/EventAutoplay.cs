﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAutoplay : MonoBehaviour {

	public string JSONScript;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.name == "Player") 
		{
			JSONObject j = new JSONObject(JSONScript);
			ScenarioQueue.queue = new Queue<JSONObject>(j["events"].list);
			ScenarioQueue.isPlaying = true;
		}	
	}
}
