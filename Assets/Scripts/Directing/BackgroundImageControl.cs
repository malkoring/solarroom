﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundImageControl : MonoBehaviour {

	public float movespeed;
	
	Vector3 pos;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float moveX = VirtualJoystick.Horizontal();
		pos = transform.position;
		transform.Translate(new Vector3(moveX * Time.deltaTime * movespeed, 0, 0));
	}
}
