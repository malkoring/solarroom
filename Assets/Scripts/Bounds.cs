using UnityEngine;

public class Bounds : MonoBehaviour {
    private BoxCollider2D bounds;
    
    void Start() {
        bounds = GetComponent<BoxCollider2D>();
        CameraController.SetBounds(bounds);
    }
}