﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area3SwitchManager : MonoBehaviour {

	public GameObject before_mokma;
	public GameObject after_mokma;
	public GameObject lightbulb;
	public GameObject crow;
	public GameObject crow_lb;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		before_mokma.SetActive(PlayData.conditions[4] ? false : true);
		after_mokma.SetActive(PlayData.conditions[4] ? true : false);
		lightbulb.SetActive(PlayData.conditions[7] ? true : false);
		crow.SetActive(PlayData.conditions[8] ? false : true);
		crow_lb.SetActive(PlayData.conditions[9] ? true : false);
	}
}
