﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area1SwitchManager : MonoBehaviour {

	public GameObject lights;
	public GameObject lightbulb;
	public GameObject sinera;
	public GameObject detection_area;
	public GameObject crow_lb;
	// Update is called once per frame
	void Update () {
		
		if(!PlayData.conditions[0])
		{
			// 카메라 자동이동이 요구되는 부분. JSON 파일 별도 수정 필요.
		}

		lights.SetActive( PlayData.conditions[1] ? false : true);
		lightbulb.SetActive( PlayData.conditions[2] ? true : false );
		sinera.SetActive( PlayData.conditions[3] ? true : false );
		crow_lb.SetActive(PlayData.conditions[10] ? true : false);
		detection_area.SetActive(PlayData.conditions[10] ? true : false);
	}
}
