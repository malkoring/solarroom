﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area2SwitchManager : MonoBehaviour {

	public GameObject lights;
	// Use this for initialization
	public GameObject mokma_leg;
	public GameObject bond;
	public GameObject detection_area;
	public GameObject crow_lb;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		lights.SetActive( PlayData.conditions[1] ? false : true );
		mokma_leg.SetActive( PlayData.conditions[5] ? false : true );
		bond.SetActive(PlayData.conditions[6] ? false : true);
		detection_area.SetActive(PlayData.conditions[11] ? true : false);
		crow_lb.SetActive(PlayData.conditions[11] ? true : false);
	}
}
