﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private Animator anim;
	private Rigidbody2D myRigidbody;
	private bool playerMoving;

	[HideInInspector]
	public static Vector2 lastMove;

	[HideInInspector]
	public bool dontMove;
	private static bool playerExists;
	public static bool autoMove;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		myRigidbody = GetComponent<Rigidbody2D>();

		if(!playerExists) {
			playerExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		float moveX = VirtualJoystick.Horizontal();
		float moveY = VirtualJoystick.Vertical();

		playerMoving = false;

		if(PlayerPrefs.GetInt("DontTouch") != 1) {
			if (moveX > 0.5f || moveX < -0.5f ) {
				myRigidbody.velocity = new Vector2(moveX * moveSpeed, myRigidbody.velocity.y);
				playerMoving = true;
				lastMove = new Vector2(moveX, 0f);
			}

			if (moveY > 0.5f || moveY < -0.5f) {
				myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, moveY * moveSpeed);
				playerMoving = true;
				lastMove = new Vector2(0f, moveY);
			}	
		

			if (moveX < 0.5f && moveX > -0.5f) {
				myRigidbody.velocity = new Vector2(0f, myRigidbody.velocity.y);
			}

			if (moveY < 0.5f && moveY > -0.5f) {
				myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, 0f);
			}
		} else {
			myRigidbody.velocity = Vector2.zero;
		}
		
		PlayData.position = transform.position;
		PlayData.faceTo = lastMove;

		if(!autoMove) {
			anim.SetFloat("MoveX", moveX);
			anim.SetFloat("MoveY", moveY);
			anim.SetBool("PlayerMoving", playerMoving);
			anim.SetFloat("LastMoveX", lastMove.x);
			anim.SetFloat("LastMoveY", lastMove.y);
		} else {
			lastMove.x = anim.GetFloat("LastMoveX");
			lastMove.y = anim.GetFloat("LastMoveY");
		}
	}

}
