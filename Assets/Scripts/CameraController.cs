﻿using UnityEngine;

public class CameraController : MonoBehaviour {
	public GameObject followTarget;
	private Vector3 targetPos;
	public float moveSpeed;

	private static bool cameraExists;
	private static BoxCollider2D boundBox;
	private static Vector3 minBound;
	private static Vector3 maxBound;
	public static bool cameraFix;

	private Camera theCamera;
	private float halfWidth;
	private float halfHeight;

	// Use this for initialization
	void Start () {
		boundBox = GameObject.Find("CameraBoundary").GetComponent<BoxCollider2D>();

		if(!cameraExists) {
			cameraExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy(gameObject);
		}

		minBound = boundBox.bounds.min;
		maxBound = boundBox.bounds.max;

		theCamera = GetComponent<Camera>();
		halfHeight = theCamera.orthographicSize;
		halfWidth = halfHeight * Screen.width / Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
		 if(Global.cameraAutoMove)
			return;

		if(!cameraFix) {
			targetPos = new Vector3(followTarget.transform.position.x, 
									followTarget.transform.position.y,
									-10);
			transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
		

			float clampedX = Mathf.Clamp(transform.position.x, 
											minBound.x + halfWidth, maxBound.x - halfWidth);
			float clampedY = Mathf.Clamp(transform.position.y, 
											minBound.y + halfHeight, maxBound.y - halfHeight);
			transform.position = new Vector3(clampedX, clampedY, transform.position.z);		
		} else {
			transform.position = new Vector3((minBound.x+maxBound.x)/2, minBound.y * (4/8.0f) + maxBound.y * (4/8.0f), transform.position.z);
		}

	}

	public static void SetBounds(BoxCollider2D newBounds)
	{
		boundBox = newBounds;
		minBound = boundBox.bounds.min;
		maxBound = boundBox.bounds.max;
	}
}
