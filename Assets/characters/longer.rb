require 'rmagick'

image_path = "resized_Actor1.png"
image = Magick::Image::read(image_path).first

image = image.crop(0, 0, image.columns/4, image.rows/2)

image.write("resized_Actor1.png")

image.destroy!
